## MOMW Gameplay Changelog

#### Version 2.9

* Added support for [Better Merchants Skills for OpenMW](https://www.nexusmods.com/morrowind/mods/54337) (`Maximum Mercantile Difference = 30`)
* Rebalanced NCGD skill growth and decay rates

<!--[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/33876115)-->

#### Version 2.8

* Added support for [Go Home! Locking Doors](https://modding-openmw.gitlab.io/go-home/locking-doors/) (`AM Hour = 7.5`)

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/33876115)

#### Version 2.7

* Added support for [Dynamic Music (OpenMW Only)](https://www.nexusmods.com/morrowind/mods/53568) (`Use Default Soundbank = true`)

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/33774414)

#### Version 2.6

* Updated for the new version of Pharis' Magicka Regeneration

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/28587445)

#### Version 2.5

* Added factory reset options for the Balance and UI plugins

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/26622628)

#### Version 2.4

* Rebalanced NCGD skill growth rates
* Disabled Sol's Lose More Health

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/25780738)

#### Version 2.3

* Added new, separate plugins for balance and UI changes

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/25350343)

#### Version 2.2

* Added support for several new mods:
    * [Bound Balance](https://modding-openmw.gitlab.io/bound-balance/)
    * [Go Home!](https://modding-openmw.gitlab.io/go-home/)
    * [Pause Control](https://modding-openmw.gitlab.io/pause-control/)
* Updated support for [NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/)
* Removed support for [MBSP Uncapped (OpenMW Lua) - NCGD Compat](https://www.nexusmods.com/morrowind/mods/53064) since it's been merged into NCGDMW

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/24374562)

#### Version 2.1

* Added the missing translation file folder

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23880244)

#### Version 2.0

* Rewrote how the mod works:
  * Changes are now only applied once, when you start a new game
  * To re-apply changes at any time, navigate to the script settings menu (ESC >> Options >> Scripts >> MOMW Gameplay) and set "Factory Reset" to "yes"

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23717287)

#### Version 1.6

* Move and resize Solthas' Sneak Step Drain meter to below the health meter

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23284658)

#### Version 1.5

* Reverted an option for [Signpost Fast Travel](https://modding-openmw.gitlab.io/signpost-fast-travel/) (show travel messages: `false`; now the default of `true` is used)

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/20994721)

#### Version 1.4

* Added options for [Zack's Lua Multimark Mod](https://www.nexusmods.com/morrowind/mods/53260) and [Signpost Fast Travel](https://modding-openmw.gitlab.io/signpost-fast-travel/)

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/20990518)

#### Version 1.3

* Fixed support for [UiModes](https://modding-openmw.gitlab.io/ui-modes/) - it's a player setting not a global setting

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/19522647)

#### Version 1.2

* Add support for [UiModes](https://modding-openmw.gitlab.io/ui-modes/) to pause when inventory is open but not when dialogue is

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/19078936)

#### Version 1.1

* Move the camera in Solthas' Sneak Drain meter

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/18476417)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/17469202)
