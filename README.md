# MOMW Gameplay

Automatically configure various mods to suit the modded setups of Modding-OpenMW.com.

**Requires OpenMW 0.48 or newer!**

This mod will, on the first launch, set various settings across several mods to values that I've found to work well after a bit of playtesting.

You can change any of the settings that the mod touches at any time. If you want to return to the recommended values from the mod, you can use the "Factory Reset" feature in the settings menu.

#### How It Works

Load this mod to receive automatic configurations for many mods (see below for the full listing).

* All of the settings are applied after you launch the game with this mod enabled for the first time
* You may change settings after this happens and they will be preserved
* You may enable the "Factory Reset" option in the script settings menu to re-apply all settings from this mod at any time
  * That's at: ESC >> Options >> Scripts >> MOMW Gameplay
* Use the "Factory Reset" feature to apply these settings into an existing save
* The UI and Balance plugins have their own settings menu, each with a "Factory Reset" option

#### Web

[Project Home](https://modding-openmw.gitlab.io/momw-gameplay/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/momw-gameplay)

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/momw-gameplay/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Settings\momw-gameplay

        # Linux
        /home/username/games/OpenMWMods/Settings/momw-gameplay

        # macOS
        /Users/username/games/OpenMWMods/Settings/momw-gameplay

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Settings\momw-gameplay"`)
1. Add `content=momw-gameplay.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher
    * Optionally also add `content=momw-gameplay-balance.omwscripts` or `content=momw-gameplay-ui.omwscripts`
1. Each time you start a new game, the following will be set if the related mod is active:
    * Always run (OpenMW builtin): **Yes**
    * Toggle sneak (OpenMW builtin): **Yes**
    * Show messages ([Action Camera Swap](https://modding-openmw.gitlab.io/action-camera-swap/)): **No**
    * Level limit ([Bound Balance](https://modding-openmw.gitlab.io/bound-balance/)): **3**
    * Maximum Mercantile Difference ([Better Merchants Skills for OpenMW](https://www.nexusmods.com/morrowind/mods/54337)): **30**
    * AM Hour ([Go Home!](https://modding-openmw.gitlab.io/go-home/)): **7.5**
    * AM Hour ([Go Home! Locking Doors](https://modding-openmw.gitlab.io/go-home/locking-doors/)): **7.5**
    * Show Intro Message ([NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/)): **No**
    * Stats Menu Key ([NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/)): **N**
    * Skill Increase From Books ([NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/)): **No**
    * Skill Increase Constant Factor ([NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/)): **Half**
    * Skill Increase Squared Level Factor ([NCGDMW Lua Edition](https://modding-openmw.gitlab.io/ncgdmw-lua/)): **Down To A Quarter**
    * Pause for dialogue ([Pause Control](https://modding-openmw.gitlab.io/pause-control/)): **No**
    * Hotbar on top ([Quickselect](https://modding-openmw.gitlab.io/quickselect/)): **Yes**
    * Show messages ([Smart Ammo](https://modding-openmw.gitlab.io/smart-ammo/)): **No**
    * Magic Shader Intensity ([Solthas Combat Pack (OpenMW Lua): Solthas Bodz Stat Shaders](https://www.nexusmods.com/morrowind/mods/52221)): **0.6**
    * Fatigue Multiplier ([Solthas Combat Pack (OpenMW Lua): Charge Attack Parry](https://www.nexusmods.com/morrowind/mods/52221)): **0.03**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Charge Attack Parry](https://www.nexusmods.com/morrowind/mods/52221)): **0**
    * Enabled ([Solthas Combat Pack (OpenMW Lua): Lose More Health](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Sneak Jump Dodge](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Staff Spell Buffs](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Swim Boost Drain](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Timed Directional Attacks](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Fatigue Multiplier ([Solthas Combat Pack (OpenMW Lua): Weighty Charge Attacks](https://www.nexusmods.com/morrowind/mods/52221)): **0.03**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Weighty Charge Attacks](https://www.nexusmods.com/morrowind/mods/52221)): **0**
    * Inventory Don't Pause ([UiModes](https://modding-openmw.gitlab.io/ui-modes/)): **false**
    * Levels Per Mark ([Zack's Lua Multimark Mod](https://www.nexusmods.com/morrowind/mods/53260)): **8**
    * Base Multiplier ([Pharis' Magicka Regeneration](https://www.nexusmods.com/morrowind/mods/52779)): **0.06**
    * Gold Per Travel Distance Unit ([Signpost Fast Travel](https://modding-openmw.gitlab.io/signpost-fast-travel/)): **10**
    * Travel Menu Show Usage ([Signpost Fast Travel](https://modding-openmw.gitlab.io/signpost-fast-travel/)): **false**
    * Use Default Soundbank ([Dynamic Music (OpenMW Only)](https://www.nexusmods.com/morrowind/mods/53568)): **true**

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/momw-gameplay/-/issues)
* Email `momw-gameplay@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`
