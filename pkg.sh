#!/bin/sh
set -e

file_name=momw-gameplay.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} \
    CHANGELOG.md \
    LICENSE \
    README.md \
    momw-gameplay.omwscripts \
    momw-gameplay-balance.omwscripts \
    momw-gameplay-ui.omwscripts \
    scripts \
    l10n \
    version.txt

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
