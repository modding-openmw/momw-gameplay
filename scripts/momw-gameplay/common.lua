local core = require('openmw.core')

local isOpenMW049 = core.API_REVISION > 29

local function checkAndSet(data, storageSection)
    if data.contentFile == nil then
        print("Missing required contentFile")
    end
    if data.key == nil then
        print("Missing required key")
        print(data.contentFile)
    end
    if data.value == nil then
        print("Missing required value")
        print(data.contentFile)
        print(data.key)
    end
    if data.section == nil then
        print("Missing required section")
        print(data.contentFile)
        print(data.key)
        print(data.value)
    end
    if isOpenMW049 and core.contentFiles.has(data.contentFile) then
        print(string.format("Setting \"%s=%s\" for \"%s\" (%s)", data.key, data.value, data.section, data.contentFile))
        storageSection(data.section):set(data.key, data.value)
    elseif not isOpenMW049 then
        print(string.format("Setting \"%s=%s\" for \"%s\" (%s)", data.key, data.value, data.section, data.contentFile))
        storageSection(data.section):set(data.key, data.value)
    end
end

return {checkAndSet = checkAndSet}
