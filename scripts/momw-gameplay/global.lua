local common = require("scripts.momw-gameplay.common")
local checkAndSet = common.checkAndSet
local storage = require('openmw.storage')

local function applyAll()
    -- Better Merchants Skills for OpenMW
    checkAndSet({
            contentFile = "bmso.omwscripts",
            section = "SettingsGlobalBMSO",
            key = "maxMercantileDifference",
            value = 30
    }, storage.globalSection)

    -- Go Home!
	checkAndSet({
            contentFile = "go-home.omwscripts",
            section = "SettingsAModGoHome",
            key = "hourAM",
            value = 7.5
    }, storage.globalSection)

    -- Go Home! Locking Doors
	checkAndSet({
            contentFile = "go-home-locking-doors.omwscripts",
            section = "SettingsTimeGoHomeLockingDoors",
            key = "hourAM",
            value = 7.5
    }, storage.globalSection)

    -- Pharis Magicka Regen
	checkAndSet({
            contentFile = "pharis-magicka-regeneration.omwscripts",
            section = "SettingsGlobalPharisMagickaRegenerationGameplay",
            key = "baseMultiplier",
            value = 0.06
    }, storage.globalSection)

    -- Signpost Fast Travel
	checkAndSet({
            contentFile = "signpost-fast-travel.omwscripts",
            section = "SettingsGlobalTravelSignpostFastTravel",
            key = "goldPerUnit",
            value = 10
    }, storage.globalSection)
	checkAndSet({
            contentFile = "signpost-fast-travel.omwscripts",
            section = "SettingsGlobalTravelSignpostFastTravel",
            key = "menuShowUsage",
            value = false
    }, storage.globalSection)

    print("MOMW Gameplay: global settings have been applied")
end

return {
    engineHandlers = {onInit = applyAll},
    eventHandlers = {momw_gameplay_ApplyAll = applyAll}
}
