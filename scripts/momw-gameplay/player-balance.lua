local async = require("openmw.async")
local I = require("openmw.interfaces")
local checkAndSet = require("scripts.momw-gameplay.common").checkAndSet
local storage = require('openmw.storage')
local MOD_ID = "MOMW_Gameplay_Balance"
local SETTINGS_KEY = 'Settings' .. MOD_ID

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "nameBalance",
    description = "descriptionBalance"
}

I.Settings.registerGroup {
    key = SETTINGS_KEY,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "BalancesettingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = 'factoryResetBalance',
            name = "factoryResetBalance_name",
            description = "factoryResetBalance_desc",
            default = false,
            renderer = 'checkbox'
        }
    }
}

local function onInit()
    -- Bound Balance
    checkAndSet({
            contentFile = "bound-balance.omwscripts",
            section = "SettingsBoundBalance",
            key = "levelLimit",
            value = "3"
    }, storage.playerSection)
    -- NCGD
    checkAndSet({
            contentFile = "ncgdmw.omwscripts",
            section = "SettingsPlayerSkillsNCGDMW",
            key = "skillIncreaseFromBooks",
            value = false
    }, storage.playerSection)
    checkAndSet({
            contentFile = "ncgdmw.omwscripts",
            section = "SettingsPlayerSkillsNCGDMW",
            key = "decayRate",
            value = "slow"
    }, storage.playerSection)
    checkAndSet({
            contentFile = "ncgdmw.omwscripts",
            section = "SettingsPlayerSkillsNCGDMW",
            key = "skillIncreaseSquaredLevelFactor",
            value = "downToAQuarter"
    }, storage.playerSection)
end

local function factoryReset(_, key)
    if key == "factoryResetBalance" and storage.playerSection(SETTINGS_KEY):get("factoryResetBalance") == true then
        onInit()
        print("MOMW Gameplay: Balance factory reset executed")
    end
end
storage.playerSection(SETTINGS_KEY):subscribe(async:callback(factoryReset))

return {engineHandlers = {onInit = onInit}}
