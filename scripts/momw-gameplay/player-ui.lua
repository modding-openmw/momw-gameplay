local async = require("openmw.async")
local I = require("openmw.interfaces")
local common = require("scripts.momw-gameplay.common")
local storage = require('openmw.storage')
local checkAndSet = common.checkAndSet
local MOD_ID = "MOMW_Gameplay_UI"
local SETTINGS_KEY = 'Settings' .. MOD_ID

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "nameUI",
    description = "descriptionUI"
}

I.Settings.registerGroup {
    key = SETTINGS_KEY,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "UIsettingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = 'factoryResetUI',
            name = "factoryResetUI_name",
            description = "factoryResetUI_desc",
            default = false,
            renderer = 'checkbox'
        }
    }
}

local function onInit()
    -- Quickselect
    checkAndSet({
            contentFile = "QuickSelect.omwscripts",
            section = "SettingsQuickSelect",
            key = "hotBarOnTop",
            value = true
    }, storage.playerSection)
    -- Sol Sneak Step Drain
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSneakStepDrain",
            key = "uiXcoord",
            value = 0.5
    }, storage.playerSection)
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSneakStepDrain",
            key = "uiYcoord",
            value = 1
    }, storage.playerSection)
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSneakStepDrain",
            key = "uiLength",
            value = 316
    }, storage.playerSection)
end

local function factoryReset(_, key)
    if key == "factoryResetUI" and storage.playerSection(SETTINGS_KEY):get("factoryResetUI") == true then
        onInit()
        print("MOMW Gameplay: UI factory reset executed")
    end
end
storage.playerSection(SETTINGS_KEY):subscribe(async:callback(factoryReset))

return {engineHandlers = {onInit = onInit}}
