local async = require("openmw.async")
local core = require('openmw.core')
local storage = require('openmw.storage')
local I = require("openmw.interfaces")
local common = require("scripts.momw-gameplay.common")

local N_code = 17
local MOD_ID = "MOMW_Gameplay"
local SETTINGS_KEY = 'Settings' .. MOD_ID
local checkAndSet = common.checkAndSet

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "name",
    description = "description"
}

I.Settings.registerGroup {
    key = SETTINGS_KEY,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "settingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = 'factoryReset',
            name = "factoryReset_name",
            description = "factoryReset_desc",
            default = false,
            renderer = 'checkbox'
        }
    }
}

local function applyAll()
    -- Builtin stuff
    checkAndSet({
            contentFile = "builtin.omwscripts",
            section = "SettingsOMWControls",
            key = "alwaysRun",
            value = true
    }, storage.playerSection)
    checkAndSet({
            contentFile = "builtin.omwscripts",
            section = "SettingsOMWControls",
            key = "toggleSneak",
            value = true
    }, storage.playerSection)

    -- Action Camera Swap
    checkAndSet({
            contentFile = "action-camera-swap.omwscripts",
            section = "SettingsPlayerActionCameraSwap",
            key = "showMessages",
            value = false
    }, storage.playerSection)

    -- Dynamic Music
    checkAndSet({
            contentFile = "DynamicMusic.omwscripts",
            section = "Settings_openmw_dynamic_music_1000_general",
            key = "GENERAL_USE_DEFAULT_SOUNDBANK",
            value = true
    }, storage.playerSection)

    -- NCGD
    checkAndSet({
            contentFile = "ncgdmw.omwscripts",
            section = "SettingsPlayerNCGDMW",
            key = "showIntro",
            value = false
    }, storage.playerSection)
    checkAndSet({
            contentFile = "ncgdmw.omwscripts",
            section = "SettingsPlayerNCGDMW",
            key = "statsMenuKey",
            value = N_code
    }, storage.playerSection)

    -- Smart Ammo
    checkAndSet({
            contentFile = "smart-ammo.omwscripts",
            section = "SettingsPlayerSmartAmmo",
            key = "showMessages",
            value = false
    }, storage.playerSection)

    -- Sol Bodz Stat Shaders
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolBodzStatShaders",
            key = "magicIntensity",
            value = 0.6
    }, storage.playerSection)

    -- Sol Charge Attack Parry
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolChargeAttackParry",
            key = "fatigueMult",
            value = 0.03
    }, storage.playerSection)

    -- Sol Charge Attack Parry
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolChargeAttackParry",
            key = "verbose",
            value = 0
    }, storage.playerSection)

    -- Sol Lose More Health
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolLoseMoreHealth",
            key = "enabled",
            value = false
    }, storage.playerSection)

    -- Sol Sneak Jump Dodge
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSneakJumpDodge",
            key = "verbose",
            value = false
    }, storage.playerSection)

    -- Sol Staff Spell Buffs
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_sol_StaffSpellBuff",
            key = "verbose",
            value = false
    }, storage.playerSection)

    -- Sol Swim Boost Drain
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSwimBoostDrain",
            key = "verbose",
            value = false
    }, storage.playerSection)

    -- Sol Timed Dir Attacks
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolTimedDirAttacks",
            key = "verbose",
            value = 0
    }, storage.playerSection)

    -- Sol Weighty Charge Attacks
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolWeightyChargeAttacks",
            key = "fatigueMult",
            value = 0.03
    }, storage.playerSection)
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolWeightyChargeAttacks",
            key = "verbose",
            value = 0
    }, storage.playerSection)

    -- Ui Modes
    checkAndSet({
            contentFile = "UiModes.omwscripts",
            section = "SettingsUiModes",
            key = "inventoryDontPause",
            value = false
    }, storage.playerSection)

    -- Zack's MultiMark
    checkAndSet({
            contentFile = "LuaMultiMark.omwaddon",
            section = "SettingsMultiMark",
            key = "levelsPerMark",
            value = 8
    }, storage.playerSection)

    print("MOMW Gameplay: player settings have been applied")
end

local function factoryReset(_, key)
    if key == "factoryReset" and storage.playerSection(SETTINGS_KEY):get("factoryReset") == true then
        applyAll()
        core.sendGlobalEvent("momw_gameplay_ApplyAll")
        print("MOMW Gameplay: factory reset executed")
    end
end
storage.playerSection(SETTINGS_KEY):subscribe(async:callback(factoryReset))

return {engineHandlers = {onInit = applyAll}}
